from django.db import models

# Create your models here.


class Url(models.Model):
    url_real = models.CharField(max_length=64)
    short = models.TextField()

    def __str__(self):
        return self.url_real


class NumSec(models.Model):
    num = models.IntegerField()

    def __str__(self):
        return self.num
