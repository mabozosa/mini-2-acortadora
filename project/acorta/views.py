from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import NumSec, Url
from django.views.decorators.csrf import csrf_exempt
from urllib.parse import unquote

# Create your views here.


@csrf_exempt
def index(request):
    if request.method == "GET":
        urls_reales = Url.objects.all()  # Recupera todas las URLs reales almacenadas
        # en la base de datos
        contexto = {'urls_reales': urls_reales}
        return render(request, "acorta/index.html", contexto)
    elif request.method == "POST":
        # (request.POST funciona como un diccionario)
        url = request.POST['url']  # coger la url introducida en el formulario
        short = request.POST['short']  # coger el recurso introducido en el formulario
        if url == "":  # Si se da esta cadena en el body es porque
            # el campo url del formulario esta vacio
            urls_reales = Url.objects.all()  # Recupera todas las URLs reales almacenadas
            # en la base de datos
            contexto = {'urls_reales': urls_reales}
            return render(request, "acorta/errorCampoUrlVacio.html", contexto, status=404)
        else:
            url_body = unquote(url)  # Decodifica la URL para poder trabajar con ella
            print(url_body)
            if (url_body.find("https://www.") == 0):  # Guarda URL sin www.
                url_body = "https://" + str(url_body.split(".")[1]) + "." + str(url_body.split(".")[2])
            elif (url_body.find("http://www.") == 0):
                url_body = "http://" + str(url_body.split(".")[1]) + "." + str(url_body.split(".")[2])
            elif (url_body.find("https://") == 0):  # se tiene en cuenta el https en la URL
                url_body = "https://" + str(url_body.split("//")[1])
            elif (url_body.find("http://") == 0):  # se tiene en cuenta el http en la URL
                url_body = "http://" + str(url_body.split("//")[1])
            else:  # se añade https a la URL
                if (url_body.find("www.") == 0):
                    url_body = "https://" + str(url_body.split(".")[1]) + "." + str(url_body.split(".")[2])
                else:
                    url_body = "https://" + url_body
            try:
                url_real = Url.objects.get(url_real=url_body)  # comprobar si la URL ya estaba almacenada
                url_real.delete()  # Se borra la url de la base de datos
            except Url.DoesNotExist:
                pass
            # almacenar una URL porque es nueva
            num_aux = len(NumSec.objects.all())  # obtiene el numero de objetos para calcular
            # el numero secuencial
            if short == "":
                num_aux += 1  # Aumenta el valor en 1 para seguir la secuencia
                num_sec = NumSec(num=num_aux)  # guarda el nuevo valor en base de datos y con ello aumenta
                # el numero de objetos para hacer bien el calculo secuencial en futuras iteraciones
                num_sec.save()
                short = num_sec.num  # short ahora vale el siguiente numero secuencial
            url_real = Url(url_real=url_body, short=short)  # se queda con el valor de la URL
            url_real.save()  # guardar la URL
            urls_reales2 = Url.objects.all()  # Recupera todas las URLs reales almacenadas
            # en la base de datos
            contexto2 = {'urls_reales': urls_reales2}
            return render(request, "acorta/index.html", contexto2)


def urls_cortas_redireccion(request, short_url):  # Gestion de la redireccion
    try:
        url = Url.objects.get(short=short_url)  # coger la URL segun el id
        return HttpResponseRedirect(url.url_real)
    except Url.DoesNotExist:  # No se encuentra la URL asociada con el valor de la acortada
        contexto = {'short': short_url}
        return render(request, "acorta/errorRedireccion.html", contexto, status=404)
