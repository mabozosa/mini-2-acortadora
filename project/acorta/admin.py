from django.contrib import admin
from .models import NumSec, Url

# Register your models here.
admin.site.register(Url)
admin.site.register(NumSec)
